﻿using System;

namespace wk3ex4
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };

            Array.Sort(colors);
            Array.Reverse(colors);

            Console.WriteLine(String.Join(", ", colors));
    }
    }
}

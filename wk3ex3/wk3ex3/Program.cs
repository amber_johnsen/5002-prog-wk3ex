﻿using System;

namespace wk3ex3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };

            var join = string.Join(",", colors);

            Console.WriteLine(join);
        }
    }
}

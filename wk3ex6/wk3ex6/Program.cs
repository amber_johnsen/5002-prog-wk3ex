﻿using System;
using System.Collections.Generic;

namespace wk3ex6
{
    class MainClass
    {
       public static void Main(string[] args)
        {
            var colors = new List<string> { "red", "blue", "orange", "white", "black" };
            colors.Add("pink");
            colors.Add("purple");
            colors.Add("light blue");

            Console.WriteLine(String.Join(", ", colors));
        }
    }
}

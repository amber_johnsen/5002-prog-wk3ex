﻿using System;

namespace wk3ex1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };

            for (var i=0; i <colors.Length; i++)
            {
                Console.WriteLine(colors[i]);
            }
        }
    }
}

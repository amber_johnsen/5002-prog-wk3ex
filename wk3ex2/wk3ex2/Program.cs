﻿using System;

namespace wk3ex2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };

            foreach (var x in colors)
            {
                Console.WriteLine(x);
            }
        }
    }
}
